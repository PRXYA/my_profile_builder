package com.example.myprofilebuilder;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class DisplayActivity extends AppCompatActivity {

    private ImageView iv_profile;
    private Button button_edit;
    TextView tv_name;
    TextView tv_gender;

    public static final String USER_KEY = "USER";
    public static final int REQ_CODE = 100;

    public static User user = new User();


    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        iv_profile = findViewById(R.id.imageProfile);
        tv_name = (TextView) findViewById(R.id.profileName);
        tv_gender = (TextView) findViewById(R.id.profileGender);

        final Bundle extrasFromMain = getIntent().getExtras().getBundle(USER_KEY);

        user = (User) extrasFromMain.getSerializable("user");

        if (user!=null){
            if (user.gender.equals("Male"))
                iv_profile.setImageResource(R.drawable.male);
            else
                iv_profile.setImageResource(R.drawable.female);
        }

            tv_name.setText("Name: " + user.first_name + " " + user.last_name);
            tv_gender.setText(user.gender);

        findViewById(R.id.buttonEdit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toEdit = new Intent(DisplayActivity.this, EditActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("bundleEdit", user);
                toEdit.putExtra("toEdit", bundle);

                startActivityForResult(toEdit,REQ_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQ_CODE){
            if(resultCode==RESULT_OK){
                final Bundle extrasFromMain = data.getExtras().getBundle(USER_KEY);

                user = (User) extrasFromMain.getSerializable("usertoDisplay");

                if (user!=null){
                    if (user.getGender().equals("Male"))
                        iv_profile.setImageResource(R.drawable.male);
                    else
                        iv_profile.setImageResource(R.drawable.female);

                    tv_name.setText("Name: " + user.first_name + " " + user.last_name);
                    tv_gender.setText(user.gender);
                }
            }
        }
    }
}

