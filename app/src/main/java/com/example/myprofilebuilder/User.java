package com.example.myprofilebuilder;

import android.util.Log;

import java.io.Serializable;

public class User implements Serializable {
    String gender;
    String first_name;
    String last_name;

    public void setGender(String gender){
        this.gender = gender;
    }

    public void setFirstName(String fname){
        this.first_name = fname;
    }

    public void setLastName(String lname){
        this.last_name = lname;
    }

    public String getGender(){
        return this.gender;
    }
}
