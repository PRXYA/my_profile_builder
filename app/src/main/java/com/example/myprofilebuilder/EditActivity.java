package com.example.myprofilebuilder;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Toast;

import static com.example.myprofilebuilder.MainActivity.USER_KEY;


public class EditActivity extends AppCompatActivity {
    RadioGroup rb_gender;
    RadioButton rb_female;
    RadioButton rb_male;
    ImageView iv_gender;

    EditText fname;
    EditText lname;

    public static User user = new User();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        final Bundle extrasFromDisplay = getIntent().getExtras().getBundle("toEdit");
        user = (User) extrasFromDisplay.getSerializable("bundleEdit");

        rb_male = findViewById(R.id.radioMale);
        rb_female = findViewById(R.id.radioFemale);
        rb_gender = findViewById(R.id.radioGroup);
        fname = findViewById(R.id.fnameInput);
        lname = findViewById(R.id.lnameInput);
        iv_gender = findViewById(R.id.imageProfile);

        if(user!=null){
            if (user.getGender().equals("Male")){
                rb_male.setChecked(true);
                iv_gender.setImageResource(R.drawable.male);

            }else{
                rb_female.setChecked(true);
                iv_gender.setImageResource(R.drawable.female);
            }
        }

        fname.setText(user.first_name);
        lname.setText(user.last_name);



        rb_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radioFemale:
                        iv_gender.setImageResource(R.drawable.female);
                        user.gender = "Female";
                        break;
                    case R.id.radioMale:
                        iv_gender.setImageResource(R.drawable.male);
                        user.gender = "Male";
                        break;
                    default:
                }
            }
        });

        findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(fname.getText().toString()== null || fname.getText().toString().length() == 0){
                    fname.setError("Please enter your first name");
                }else if(lname.getText().toString() == null || lname.getText().toString().length() == 0){
                    lname.setError("Please enter your last name");
                }else if(user.gender == null || user.gender.length() == 0){
                    Toast.makeText(getApplicationContext(), "Please select your gender",  Toast.LENGTH_LONG).show();
                } else {
                    user.first_name = fname.getText().toString();
                    user.last_name = lname.getText().toString();

                    Bundle sentData = new Bundle();

                    sentData.putSerializable("usertoDisplay", user);

                    Intent intent = new Intent(EditActivity.this, DisplayActivity.class);
                    intent.putExtra(USER_KEY, sentData);

                    setResult(EditActivity.RESULT_OK, intent);
                    finish();
                }
            }
        });

    }

}
