package com.example.myprofilebuilder;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    RadioGroup rb_gender;
    RadioButton rb_female;
    RadioButton rb_male;

    ImageView iv_gender;

    String selectedGender;

    EditText fname;
    EditText lname;

    public static final String USER_KEY = "USER";
    public static User user = new User();

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rb_gender = findViewById(R.id.radioGroup);
        rb_female = findViewById(R.id.radioFemale);
        rb_male = findViewById(R.id.radioMale);
        iv_gender = findViewById(R.id.imageProfile);

        rb_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radioFemale:
                        iv_gender.setImageResource(R.drawable.female);
                        selectedGender = "Female";
                        break;
                    case R.id.radioMale:
                        iv_gender.setImageResource(R.drawable.male);
                        selectedGender = "Male";
                        break;
                    default:
                }
            }
        });

        fname = (EditText) findViewById(R.id.fnameInput);
        lname = (EditText) findViewById(R.id.lnameInput);

        findViewById(R.id.buttonSave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(fname.getText().toString()== null || fname.getText().toString().length() == 0){
                    fname.setError("Please enter your first name");
                }else if(lname.getText().toString() == null || lname.getText().toString().length() == 0){
                    lname.setError("Please enter your last name");
                }else if(selectedGender == null || selectedGender.length() == 0){
                    Toast.makeText(getApplicationContext(), "Please select your gender",  Toast.LENGTH_LONG).show();
                } else {
                    Intent intent = new Intent(MainActivity.this, DisplayActivity.class);

                    user.gender = selectedGender;
                    user.first_name = fname.getText().toString();
                    user.last_name = lname.getText().toString();

                    Bundle sentData = new Bundle();
                    sentData.putSerializable("user",user);

                    intent.putExtra(USER_KEY, sentData);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }
}
